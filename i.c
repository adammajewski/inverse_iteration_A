/*

 gcc i.c -lm -Wall
 ./a.out


iPeriodChild = 1 , c = (0.250000, 0.000000); z = (-0.0000000000000000, -0.5000000000000000) 
 iPeriodChild = 2 , c = (-0.750000, 0.000000); z = (-0.0000000000000001, 0.3406250193166067) z = 0.000000000000000  -0.340625019316607 i
 iPeriodChild = 3 , c = (-0.125000, 0.649519); z = (-0.2299551351162812, -0.1413579816050057) z = -0.229955135116281  -0.141357981605006 i
 iPeriodChild = 4 , c = (0.250000, 0.500000); z = (-0.2288905993372874, -0.0151096456992674) 
 iPeriodChild = 5 , c = (0.356763, 0.328582); z = (-0.1990400075391210, 0.0415980651776321) 
 iPeriodChild = 6 , c = (0.375000, 0.216506); z = (-0.1727194378627304, 0.0675726990190151) 
 iPeriodChild = 7 , c = (0.367375, 0.147184); z = (-0.1530209385352789, 0.0799609106267383) 
 iPeriodChild = 8 , c = (0.353553, 0.103553); z = (-0.1386555899358813, 0.0860089512209437) 
 iPeriodChild = 9 , c = (0.339610, 0.075192); z = (-0.1281114080080390, 0.0889429110652104) z = -0.128111408008039  +0.088942911065210 i


*/


#include <stdio.h>
#include <math.h> // M_PI; needs -lm also 
#include <complex.h>



/* find c in component of Mandelbrot set 
 
   uses code by Wolf Jung from program Mandel
   see function mndlbrot::bifurcate from mandelbrot.cpp
   http://www.mndynamics.com/indexp.html

*/
double complex GiveC(double InternalAngleInTurns, double InternalRadius, unsigned int Period)
{
  //0 <= InternalRay<= 1
  //0 <= InternalAngleInTurns <=1
  double t = InternalAngleInTurns *2*M_PI; // from turns to radians
  double R2 = InternalRadius * InternalRadius;
  double Cx, Cy; /* C = Cx+Cy*i */

  switch ( Period ) // of component 
    {
    case 1: // main cardioid
      Cx = (cos(t)*InternalRadius)/2-(cos(2*t)*R2)/4; 
      Cy = (sin(t)*InternalRadius)/2-(sin(2*t)*R2)/4; 
      break;
    case 2: // only one component 
      Cx = InternalRadius * 0.25*cos(t) - 1.0;
      Cy = InternalRadius * 0.25*sin(t); 
      break;
      // for each iPeriodChild  there are 2^(iPeriodChild-1) roots. 
    default: // higher periods : to do, use newton method 
      Cx = 0.0;
      Cy = 0.0; 
      break; }

  return Cx + Cy*I;
}





/* mndyncxmics::root from mndyncxmo.cpp  by Wolf Jung (C) 2007-2014. */

// input = x,y
// output = u+v*I = sqrt(x+y*i) 
complex double GiveRoot(complex double z)
{  
  double x = creal(z);
  double y = cimag(z);
  double u, v;
  
   v  = sqrt(x*x + y*y);

   if (x > 0.0)
        { u = sqrt(0.5*(v + x)); v = 0.5*y/u; return  u+v*I; }
   if (x < 0.0)
         { v = sqrt(0.5*(v - x)); if (y < 0.0) v = -v; u = 0.5*y/v; return  u+v*I; }
   if (y >= 0.0) 
       { u = sqrt(0.5*y); v = u; return  u+v*I; }


   u = sqrt(-0.5*y); 
   v = -u;
   return  u+v*I;
}




// from mndlbrot.cpp  by Wolf Jung (C) 2007-2014. part of Madel 5.12 
// input : c, z , mode
// c = cx+cy*i where cx and cy are global variables defined in mndynamo.h
// z = x+y*i
// 
// output : z = x+y*i

/*

"No,  the argument adjusting in the inverse branch has nothing to do with computing external arguments.  It is related to itineraries and kneading sequences, 
 however:  At the critical point 0,  you can cut the Julia sets in two parts A and B,  such that the critical orbit stays in these parts.  
(It does not go to the parts at the upper and lower side of 0.)  
As a quick and dirty approximation to these parts,  I am assuming that the line perpendicular to c can be taken to separate A and B.

If you are drawing the Julia set by taking all preimages,  there is no need to do this adjustment.
------------------
When you write the real and imaginary parts in the formulas as complex numbers again,
you see that it is
sqrt( -c / |c|^2 )  *  sqrt( |c|^2 - conj(c)*z ) ,
so this is just sqrt( z - c )  except for the overall sign:
the standard square-root takes values in the right halfplane,  but this is rotated by the squareroot of -c .
The new line between the two planes has half the argument of -c .
(It is not orthogonal to c,  which I said earlier.)

" Wolf Jung
*/
complex double InverseIteration(complex double z, complex double c)
{
    double x = creal(z);
    double y = cimag(z);
    double cx = creal(c);
    double cy = cimag(c);
   
   // f^{-1}(z) = inverse with principal value
   if (cx*cx + cy*cy < 1e-20) 
   {  
      z = GiveRoot(x - cx + (y - cy)*I); // 2-nd inverse function = key b 
      //if (mode & 1) { x = -x; y = -y; } // 1-st inverse function = key a   
      return -z;
   }
    
   //f^{-1}(z) =  inverse with argument adjusted

   double u, v;
   complex double uv ;
   double w = cx*cx + cy*cy;
    
   uv = GiveRoot(-cx/w -(cy/w)*I); 
   u = creal(uv);
   v = cimag(uv);
   //
   z =  GiveRoot(w - cx*x - cy*y + (cy*x - cx*y)*I);
   x = creal(z);
   y = cimag(z);
   //
   w = u*x - v*y; 
   y = u*y + v*x; 
   x = w;
   // 2-nd inverse function = key b 
   //if (mode & 1) // mode = -1
     //  { x = -x; y = -y; } // 1-st inverse function = key a
  
  return -x-y*I;

}


// make iPeriod inverse iteration with negative sign ( a in Wolf Jung notation )
complex double GivePrecriticalA(complex double z, complex double c, int iPeriod)
{
  complex double za = z;  
  int i; 
  for(i=0;i<iPeriod ;++i){
    
    za = InverseIteration(za,c); 
    //printf("i = %d ,  z = (%f, %f) \n ", i,  creal(z), cimag(z) );

   }

 return za;
}

int main(){
  
 complex double c;
 complex double z;
 complex double zcr = 0.0; // critical point

 int iPeriodChild;
 int iPeriodChildMax = 10; // period of
 int iPeriodParent = 1; 
  
 



 for(iPeriodChild=1;iPeriodChild<iPeriodChildMax ;++iPeriodChild) {

     c = GiveC(1.0/((double) iPeriodChild), 1.0, iPeriodParent); // root point = The unique point on the boundary of a mu-atom of period P where two external angles of denominator = (2^P-1) meet.
     z = GivePrecriticalA( zcr, c, iPeriodChild);
     printf("iPeriodChild = %d , c = (%f, %f); z = (%.16f, %.16f) \n ", iPeriodChild, creal(c), cimag(c), creal(z), cimag(z) );
}





return 0; 
} 
